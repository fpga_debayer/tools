#include <stdlib.h>
#include <stdio.h>

#include "config.h"

int main()
{
	for (int line = 0; line < FRAME_HEIGHT; line++) {
		if (line < FRAME_HEIGHT / 3) {
			for(int px = 0; px < FRAME_WIDTH; px++) {
				if (px < FRAME_WIDTH / 3) {
					putchar(0xff);
					putchar(0x00);
					putchar(0x00);
				} else if (px < (FRAME_WIDTH / 3) * 2) {
					putchar(0x00);
					putchar(0xff);
					putchar(0x00);
				} else {
					putchar(0x00);
					putchar(0x00);
					putchar(0xff);
				}
			}
		} else if (line < (FRAME_HEIGHT / 3) * 2) {
			for(int px = 0; px < FRAME_WIDTH; px++) {
				if (px < FRAME_WIDTH / 3) {
					putchar(0x00);
					putchar(0x00);
					putchar(0xff);
				} else if (px < (FRAME_WIDTH / 3) * 2) {
					putchar(0xff);
					putchar(0x00);
					putchar(0x00);
				} else {
					putchar(0x00);
					putchar(0xff);
					putchar(0x00);
				}
			}
		} else {
			for(int px = 0; px < FRAME_WIDTH; px++) {
				if (px < FRAME_WIDTH / 3) {
					putchar(0x00);
					putchar(0xff);
					putchar(0x00);
				} else if (px < (FRAME_WIDTH / 3) * 2) {
					putchar(0x00);
					putchar(0x00);
					putchar(0xff);
				} else {
					putchar(0xff);
					putchar(0x00);
					putchar(0x00);
				}
			}
		}
	}
	return EXIT_SUCCESS;
}
