#!/bin/sh

convert -size 12x12 xc:white -draw 'fill rgb(255,0,0) rectangle 0,0 3,12' -draw 'fill rgb(0,255,0) rectangle 4,0 7,12' -draw 'fill rgb(0,0,255) rectangle 8,0 12,12' png24:test.png
