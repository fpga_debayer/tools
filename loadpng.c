#include <stdlib.h>
#include <stdio.h>
#include <png.h>

#include "config.h"


int main(int argc, char **argv)
{

	FILE *fp;
	int width, height;
	png_byte color_type;
	png_byte bit_depth;
	png_bytep * row_pointers;
	png_structp png;
	png_infop info;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s filename\n", argv[0]);
		return EXIT_FAILURE;
	}

	if ((fp = fopen(argv[1], "rb")) == NULL) {
		perror("Error while opening the file.\n");
	}

	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png) {
		fprintf(stderr, "Failed to init libpng\n");
		return EXIT_FAILURE;
	}


	info = png_create_info_struct(png);
	if(!info) {
		fprintf(stderr, "Failed to create info struct\n");
		return EXIT_FAILURE;
	}

	if(setjmp(png_jmpbuf(png))) {
		fprintf(stderr, "setjmp failure");
		return EXIT_FAILURE;
	}

	png_init_io(png, fp);

	png_read_info(png, info);

	width      = png_get_image_width(png, info);
	height     = png_get_image_height(png, info);
	color_type = png_get_color_type(png, info);
	bit_depth  = png_get_bit_depth(png, info);

	row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
	for (int y=0; y<height; y++) {
		row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png, info));
	}

	png_read_image(png, row_pointers);

	fclose(fp);

	for (int y=0; y<height; y++) {
		png_byte* row = row_pointers[y];
		for (int x=0; x<width; x++) {
			png_byte* ptr = &(row[x*3]);
			putchar(ptr[0]);
			putchar(ptr[1]);
			putchar(ptr[2]);
		}
	}
}
