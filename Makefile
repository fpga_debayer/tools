all: draw loadpng rgb2bayer gentestimg

draw: draw.c config.h
	$(CC) -o draw draw.c -lSDL2

loadpng: loadpng.c config.h
	$(CC) -o loadpng loadpng.c -lpng

rgb2bayer: rgb2bayer.c config.h
	$(CC) -o rgb2bayer rgb2bayer.c

gentestimg: gentestimg.c config.h
	$(CC) -o gentestimg gentestimg.c

clean:
	rm draw loadpng rgb2bayer gentestimg
