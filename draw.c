#include <stdlib.h>
#include <SDL2/SDL.h>

#include "config.h"

int yuv_to_rgb(int y, int u, int v)
{
        unsigned int pixel32 = 0;
        unsigned char *pixel = (unsigned char *)&pixel32;
        int r, g, b;
        r = y + (1.370705 * (v-128));
        g = y - (0.698001 * (v-128)) - (0.337633 * (u-128));
        b = y + (1.732446 * (u-128));
        if(r > 255) r = 255;
        if(g > 255) g = 255;
        if(b > 255) b = 255;
        if(r < 0) r = 0;
        if(g < 0) g = 0;
        if(b < 0) b = 0;
        pixel[0] = r;
        pixel[1] = g;
        pixel[2] = b;
        return pixel32;
}

int main(int argc, char **argv)
{
	SDL_Event event;
	SDL_Renderer *renderer;
	SDL_Surface *surface;

	enum draw_mode mode;

	FILE *fp;

	if (argc != 3) {
		fprintf(stderr, "Usage: %s filename mode\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (!strcmp(argv[2], "rgb888"))
		mode = MODE_RGB888;
	else if (!strcmp(argv[2], "rgb8888"))
		mode = MODE_RGB8888;
	else if (!strcmp(argv[2], "bayer8"))
		mode = MODE_BAYER8;
	else if (!strcmp(argv[2], "yuyv"))
		mode = MODE_YUYV;
	else {
		fprintf(stderr, "Unknown render mode\n");
		return EXIT_FAILURE;
	}

	if ((fp = fopen(argv[1], "r")) == NULL) {
		perror("Error while opening the file.\n");
		return EXIT_FAILURE;
	}

	SDL_Init(0);
	surface = SDL_CreateRGBSurfaceWithFormat(0, FRAME_WIDTH * FRAME_SCALE, FRAME_HEIGHT * FRAME_SCALE, 32, SCROT_FORMAT);
	renderer = SDL_CreateSoftwareRenderer(surface);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);

	switch(mode) {

	case MODE_BAYER8:
		{
			u_int8_t frame[FRAME_HEIGHT][FRAME_WIDTH];

			fread(frame, 1, FRAME_SIZE, fp);

			for(int x = 0; x < FRAME_WIDTH; x++) {
				for (int y = 0; y < FRAME_HEIGHT; y++) {

					if (!(y % 2)) {
						if (!(x % 2))
							SDL_SetRenderDrawColor(renderer, frame[y][x], 0, 0, 255);
						else
							SDL_SetRenderDrawColor(renderer, 0, frame[y][x], 0, 255);
					} else {
						if (!(x % 2))
							SDL_SetRenderDrawColor(renderer, 0, frame[y][x], 0, 255);
						else
							SDL_SetRenderDrawColor(renderer, 0, 0, frame[y][x], 255);
					}

					for (int s_y = 0; s_y < FRAME_SCALE; s_y++) {
						for (int s_x = 0; s_x < FRAME_SCALE; s_x++) {
								SDL_RenderDrawPoint(renderer,
										x * FRAME_SCALE + s_x,
										y * FRAME_SCALE + s_y);
						}
					}
				}
			}
			break;
		}


	case MODE_RGB888:
		{
			u_int8_t *frame = malloc(3 * FRAME_SIZE);

			fread(frame, 3, FRAME_SIZE, fp);

			for (int y = 0; y < FRAME_HEIGHT; y++) {
				for(int x = 0; x < FRAME_WIDTH; x++) {

					SDL_SetRenderDrawColor(renderer,
							*(frame + (FRAME_WIDTH * y * 3) + (3 * x) + C_IDX_R),
							*(frame + (FRAME_WIDTH * y * 3) + (3 * x) + C_IDX_G),
							*(frame + (FRAME_WIDTH * y * 3) + (3 * x) + C_IDX_B),
							255);
					for (int s_y = 0; s_y < FRAME_SCALE; s_y++) {
						for (int s_x = 0; s_x < FRAME_SCALE; s_x++) {
								SDL_RenderDrawPoint(renderer,
										x * FRAME_SCALE + s_x,
										y * FRAME_SCALE + s_y);
						}
					}
				}
			}

			break;
		}

	case MODE_RGB8888:
		{
			u_int8_t *frame = malloc(4 * FRAME_HEIGHT * FRAME_WIDTH);

			fread(frame, 4, FRAME_SIZE, fp);

			for (int y = 0; y < FRAME_HEIGHT; y++) {
				for(int x = 0; x < FRAME_WIDTH; x++) {

					SDL_SetRenderDrawColor(renderer,
							*(frame + (FRAME_WIDTH * y * 4) + (4 * x) + C_IDX_R),
							*(frame + (FRAME_WIDTH * y * 4) + (4 * x) + C_IDX_G),
							*(frame + (FRAME_WIDTH * y * 4) + (4 * x) + C_IDX_B),
							255);

					/*
					printf("line: %d pixel: %d r: 0x%x g:0x%x b:0x%x\n", y, x,
							*(frame + FRAME_WIDTH * y + (4 * x) + C_IDX_R),
							*(frame + FRAME_WIDTH * y + (4 * x) + C_IDX_G),
							*(frame + FRAME_WIDTH * y + (4 * x) + C_IDX_B));
					*/

					for (int s_y = 0; s_y < FRAME_SCALE; s_y++) {
						for (int s_x = 0; s_x < FRAME_SCALE; s_x++) {
								SDL_RenderDrawPoint(renderer,
										x * FRAME_SCALE + s_x,
										y * FRAME_SCALE + s_y);
						}
					}
				}
			}

			break;
		}

	case MODE_YUYV:
		{
			u_int8_t frame[FRAME_HEIGHT][FRAME_WIDTH*2];

			fread(frame, 2, FRAME_SIZE, fp);

			for (int y = 0; y < FRAME_HEIGHT; y++) {
				for(int x = 0; x < FRAME_WIDTH * 2; x += 4) {

					u_int8_t _u, _v, _y0, _y1;
					int rgb_pixel;

					_y0 = frame[y][x + 0];
					_u  = frame[y][x + 1];
					_y1 = frame[y][x + 2];
					_v  = frame[y][x + 3];

					rgb_pixel = yuv_to_rgb(_y0, _u, _v);

					SDL_SetRenderDrawColor(renderer,
							(rgb_pixel & 0x000000ff),
							(rgb_pixel & 0x0000ff00) >> 8,
							(rgb_pixel & 0x00ff0000) >> 16,
							255);
					SDL_RenderDrawPoint(renderer, x/2, y);

					rgb_pixel = yuv_to_rgb(_y1, _u, _v);

					SDL_SetRenderDrawColor(renderer,
							(rgb_pixel & 0x000000ff),
							(rgb_pixel & 0x0000ff00) >> 8,
							(rgb_pixel & 0x00ff0000) >> 16,
							255);
					SDL_RenderDrawPoint(renderer, x/2 + 1, y);
				}
			}

			break;
		}
	}

	SDL_RenderPresent(renderer);

	SDL_SaveBMP(surface, "out.bmp");
	SDL_FreeSurface(surface);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
	return EXIT_SUCCESS;
}
