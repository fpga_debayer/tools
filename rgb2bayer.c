#include <stdlib.h>
#include <stdio.h>

#include "config.h"

//#define HUMAN_DBG

int main(int argc, char **argv)
{
	u_int8_t frame[FRAME_HEIGHT][FRAME_WIDTH][3];
	FILE *fp;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s filename\n", argv[0]);
		return EXIT_FAILURE;
	}

	if ((fp = fopen(argv[1], "r")) == NULL) {
		perror("Error while opening the file.\n");
	}

	fread(frame, 3, FRAME_SIZE, fp);

	for (int line = 0; line < FRAME_HEIGHT; line++) {
		for(int px = 0; px < FRAME_WIDTH; px++) {
			if (!(line % 2)) {
				if (!(px % 2))
					putchar(frame[line][px][C_IDX_R]);
				else
					putchar(frame[line][px][C_IDX_G]);
			} else {
				if (!(px % 2))
					putchar(frame[line][px][C_IDX_G]);
				else
					putchar(frame[line][px][C_IDX_B]);
			}
		}
	}

	return EXIT_SUCCESS;
}
