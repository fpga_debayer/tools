#ifndef __CONFIG_H
#define __CONFIG_H

#define FRAME_WIDTH  (512)
#define FRAME_HEIGHT (768)
#define FRAME_SCALE  (1)
#define SCROT_FORMAT SDL_PIXELFORMAT_ARGB8888

#define FRAME_SIZE   (FRAME_WIDTH * FRAME_HEIGHT)

enum color_idx {
	C_IDX_R = 0,
	C_IDX_G = 1,
	C_IDX_B = 2,
};

enum draw_mode {
	MODE_RGB888,
	MODE_RGB8888,
	MODE_BAYER8,
	MODE_YUYV,
};


#endif /* __CONFIG_H */
