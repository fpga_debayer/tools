#!/bin/sh

./loadpng $1 > input.rgb

./draw input.rgb rgb888
convert out.bmp stage1.png

./rgb2bayer parrots.rgb > parrots.raw
./draw parrots.raw bayer8
convert out.bmp stage2.png

cp parrots.raw ../linebuffer-chisel/frame.bin
