#!/bin/sh

GR='\033[0;32m'
RD='\033[0;31m'
NC='\033[0m'

log() {
	echo -e "${GR}${1}${NC}"
}

loge() {
	echo -e "${RD}${1}${NC}"
}

# File location
BASE_DIR=$(readlink -f $(dirname "${BASH_SOURCE[0]}"))
UBOOT_DIR="${BASE_DIR}/../u-boot"
LINUX_DIR="${BASE_DIR}/../linux"
SSHFS_DIR="${BASE_DIR}/../mnt"
VIVADO_DIR="${BASE_DIR}/../fpga_debayer_vivado"
VIVADO_PROJ="zybo_demosaicker"

# Board connection
SSH_USER="root"
ZYBO_IP="192.168.111.2"
SSH="${SSH_USER}@${ZYBO_IP}"

log "########### ZYBO FLASH ##########"

# Set up sshfs
if ! grep -qs "${SSH}" /proc/mounts ; then
	log "sshfs not mounted, mounting..."
	sshfs "${SSH}:/" "${SSHFS_DIR}" || { loge "sshfs failed!" && exit 1; }
fi

log "Installing uImage.."
cp ${LINUX_DIR}/arch/arm/boot/uImage ${SSHFS_DIR}/boot/
log "Installing dtb..."
cp ${LINUX_DIR}/arch/arm/boot/dts/zynq-zybo.dtb ${SSHFS_DIR}/boot/
log "Installing u-boot..."
cp ${UBOOT_DIR}/spl/boot.bin ${SSHFS_DIR}/boot/
cp ${UBOOT_DIR}/u-boot.img ${SSHFS_DIR}/boot/
log "Installing bitstream..."
cp ${VIVADO_DIR}/${VIVADO_PROJ}/${VIVADO_PROJ}.runs/impl_1/${VIVADO_PROJ}_bd_wrapper.bit ${SSHFS_DIR}/boot/download.bit


export INSTALL_MOD_PATH=${SSHFS_DIR}
export ARCH=arm

log "Installing kmods..."
make -C ${LINUX_DIR} modules_install

log "Rebooting target..."
ssh "${SSH}" reboot

log "############## DONE! #############"

